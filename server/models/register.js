module.exports = (sequelize, DataTypes) => {
  const Register = sequelize.define("Register", {
    title: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  });

  Register.associate = (models) => {};

  return Register;
};
